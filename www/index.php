<?php

/**
 * @file
 * Git API example.
 */

require_once 'config.php';

/**
 * Show message in red color.
 *
 * @param string $message
 *   The message to display.
 */
function show_message_red($message) {
  echo '<div style="color:red">';
  echo '[' . $message . ']';
  echo '</div>';
  echo '<br/>';
}

/**
 * If you need to force globally - just change the $force default value.
 *
 * @param string $action
 *   The API action to perform.
 * @param array $params
 *   The API action parameters.
 * @param bool $force
 *   Force to call API instead of using cached results.
 *
 * @return array
 *   The response data array.
 */
function gitlab($action, array $params = [], $force = FALSE) {
  $url_parts = parse_url(API_URL);
  $domain = str_replace('.', '-', $url_parts['host']);
  $action_s = str_replace('/', '_', $action);
  $action_s = str_replace(' ', '-', $action_s);
  $fname = 'data-' . $domain . '-' . $action_s . '_' . date('Y-m-d') . '.json';
  if (!$force && DEBUG && file_exists($fname)) {
    $fp = fopen($fname, 'r');
    $response = fread($fp, filesize($fname));
    fclose($fp);

    // Add from cache info message.
    static $loaded_message = FALSE;
    if (!$loaded_message) {
      show_message_red("Loaded from cache (files)");
      $loaded_message = TRUE;
    }
  }
  else {
    $curl = curl_init();
    $params_s = !empty($params) ? '?' . http_build_query($params, '', '&amp;') : '';
    curl_setopt_array($curl, array(
      CURLOPT_URL => API_URL . $action . $params_s,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_POSTFIELDS => "",
      CURLOPT_HTTPHEADER => array(
        "private-token: " . PRIVATE_TOKEN,
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    }
    else {
      if (DEBUG && !file_exists($fname)) {
        $fp = fopen($fname, 'w');
        fwrite($fp, $response);
        fclose($fp);
      }
    }
  }
  return json_decode($response);
}

/**
 * Get groups.
 */
function get_groups($search = '') {
  return gitlab("/groups", ['search' => $search]);
}

/**
 * Get group projects.
 */
function get_group_projects($group_name) {
  return gitlab("/groups/$group_name/projects", []);
}

/**
 * Get fields.
 */
function get_fields(array $objects, $field) {
  return array_map(function ($object) use ($field) {
    return $object->{$field};
  }, $objects);
}

/**
 * Render results array.
 */
function render_array(array $array, $level = 0) {
  foreach ($array as $key => $element) {
    $spacer = '&nbsp;&nbsp;';
    $tab = str_repeat($spacer, $level);
    if (is_string($key)) {
      echo $tab;
      echo '<b>' . $key . '</b>';
      echo "<br/>";
    }
    if (is_array($element)) {
      $level++;
      render_array($element, $level);
      $level--;
    }
    else {
      echo $tab;
      $not_string = !is_string($element) ? '(not string!!!) ' : '';
      echo $not_string;
      echo $element;
      echo "<br/>";
    }
  }

}

// App code.
show_message_red('API_URL: ' . API_URL);

// TODO: fix nested groups rendering.
// $groups = get_groups('blendify');

$groups = get_groups();
$group_ids = get_fields($groups, 'id');
$group_names = get_fields($groups, 'name');
$tree = [];
foreach ($group_ids as $idx => $group_id) {
  $projects = get_group_projects($group_id);
  $project_names = get_fields($projects, 'path');
  $tree[$group_names[$idx]] = $project_names;
}
render_array($tree);
